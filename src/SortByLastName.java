import java.util.Comparator;
import java.util.List;

public class SortByLastName implements SortStrategy {
    @Override
    public void sort(List<Human> list) {
        list.stream()
                .sorted(Comparator.comparing(Human::getLastName))
                .forEach(System.out::println);
    }
}
