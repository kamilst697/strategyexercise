import java.util.List;

public interface SortStrategy {
    void sort(List<Human> list);
}
