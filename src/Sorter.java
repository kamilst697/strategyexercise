import java.util.List;

public class Sorter {
    private SortStrategy strategy;

    public Sorter(SortStrategy strategy) {
        this.strategy = strategy;
    }

    public void sortHuman(List<Human> list) {
        strategy.sort(list);
    }
}
