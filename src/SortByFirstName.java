import java.util.Comparator;
import java.util.List;

public class SortByFirstName implements SortStrategy {
    @Override
    public void sort(List<Human> list) {
        list.stream()
                .sorted(Comparator.comparing(Human::getFirstName))
                .forEach(System.out::println);

    }
}
