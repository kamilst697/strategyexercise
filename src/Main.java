import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Human> humanList = generateHumanList();
        Sorter sorter = new Sorter(new SortByFirstName());
        sorter.sortHuman(humanList);
    }

    private static List<Human> generateHumanList() {
        return List.of(
                new Human("Anna", "Mazur"),
                new Human("Piotr", "Nowak"),
                new Human("Adam", "Kowalski")
        );
    }
}